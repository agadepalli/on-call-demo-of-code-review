/** 
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition. 
 *
 * All test methods in an organization are executed whenever Apex code is deployed 
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
@isTest(seeAllData=false)
private class OrderLineItemTriggerHelperTest {  
       
    
    /*
     * Method to explicitly test the "updateTopProductCategories" method of the trigger helper. To achieve the desired test result, the following methodology is employed:
        1. Purcahse order and product lot data is set up.
        2. Line Item records are inserted. As the after insert trigger fires, the purchased amount field is updated.
        3. The Top Product Cateogory feilds are explicitly set to null(for further testing) and updated back.
        4. The list of line items are used to call the trigger helper method explicitly and the behaviour of the method is tested.
    */
    static testmethod void updateTopProductCategoriesTest(){ 
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
        insert testInventoryManager;
        Lot__c prod1;
        Lot__c prod2;
        
        List<Line_Item__c> lineItemsList;
        System.runAs(testInventoryManager){
            prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
            insert prod1;
            prod2 = UnitTestHelper.reusableProductLot(250,25,'Case Covers','Iphone Silicone Case');
            insert prod2;
        }
        System.runAs(testAccountManager){
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
            insert odr;
            Test.startTest();
                lineItemsList = new List<Line_Item__c>();
                Line_Item__c lItem1 = UnitTestHelper.reusableLineItem(prod1.Id,odr.Id,50);              
                Line_Item__c lItem2 = UnitTestHelper.reusableLineItem(prod2.Id,odr.Id,150); 
                lineItemsList.add(lItem1);
                lineItemsList.add(lItem2);
                insert lineItemsList;
                Order__c nullifyOrderParams = new Order__c(Id=odr.Id,Product_Category_1__c=null,Product_Category_2__c=null,Product_Category_Units_1__c=null,Product_Category_Units_2__c=null,Purchased__c = 0);
                update nullifyOrderParams;
                OrderLineItemTriggerHelper.updateOrders(lineItemsList);
            Test.stopTest();
            Order__c updatedPurchaseOrder = [Select Remaining__c,Product_Category_1__c,Product_Category_2__c,Product_Category_Units_1__c,Product_Category_Units_2__c,Purchased__c from Order__C where Id =:odr.Id];
            System.assertEquals(updatedPurchaseOrder.Product_Category_1__c,'Iphone Silicone Case');
            System.assertEquals(updatedPurchaseOrder.Product_Category_2__c,'HTC');
            System.assertEquals(updatedPurchaseOrder.Product_Category_Units_1__c,150);
            System.assertEquals(updatedPurchaseOrder.Product_Category_Units_2__c,50);
            System.assertEquals(updatedPurchaseOrder.Purchased__c,4000);
        }
    }
    
}