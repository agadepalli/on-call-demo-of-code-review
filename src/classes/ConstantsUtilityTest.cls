/** 
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition. 
 *
 * All test methods in an organization are executed whenever Apex code is deployed 
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
@isTest(seeAllData = false)
private class ConstantsUtilityTest {   
      
    
    /*
     * Method to assert the string literals/constants declared
    */
    static testmethod void testConstantValues(){ 
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        
        System.runAs(testAccountManager){
            Test.startTest();
            	System.assertEquals(ConstantsUtility.ALL,'All');
            	System.assertEquals(ConstantsUtility.DRAFT,'Draft');
            	System.assertEquals(ConstantsUtility.ANY_CATEGORY,'--Any Category--');
            	System.assertEquals(ConstantsUtility.LKID,'_lkid');
            	System.assertEquals(ConstantsUtility.CF,'CF');
            	System.assertEquals(ConstantsUtility.BUDGET,'Budget: ');
            	System.assertEquals(ConstantsUtility.STORE,'Store: ');
            	System.assertEquals(ConstantsUtility.PONAME,'Purchase Order Name: ');
            Test.stopTest();           
        }
    }    
}